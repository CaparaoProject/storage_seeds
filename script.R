library(dplyr)
library(readr)
armz <- read_csv2("data/armz.csv")
head(armz)
armz %>% rename(`V-20` = `v-20`,
                `t5010` = T5010) %>%
    mutate(MASS = log(MASS + 1)) %>% write.csv("./data/armz_mod.csv")
#la otra tabla
armz_rep <- read_csv2("data/armz_rep.csv")

resumo <- armz_rep %>%
    group_by(`Species code`) %>%
    select(-c(1:5)) %>%
    summarise_at(-1, .funs = function(x) mean(x, na.rm = T))
res <- armz_rep %>% select(1:5) %>% distinct()
resumo <- left_join(res, resumo)
write.csv(resumo, "./data/armz_resumo.csv")
